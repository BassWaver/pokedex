import requests, threading, time, json, os

class PokeAPI(object):
    def __init__(this, delay = 1):
        this.url = "https://pokeapi.co/api/v2"
        this.cache = Cache()
    def get(this, type, query, bypass = False, **kwargs):
        params = {**kwargs}
        if this.cache[str(type)+str(query)] is not None and not bypass:
            return this.cache[str(type)+str(query)]
        response = requests.get("{}/{}/{}".format(this.url, type, query), params = params)
        if response.status_code is 200:
            this.cache[str(type)+str(query)] = response
            this.cache[str(type)+str(query)] = json.loads(response.text)
            return json.loads(response.text)
        return None

class Cache(object):
    def __init__(this, upperLimit = 500, delay = 120):
        this.map = {}
        this.monitoring = True
        this.monitorFlag = False
        this.upperLimit = upperLimit
        this.delay = delay
        monitorThread = threading.Thread(target=this.monitor, args=())
        monitorThread.start()
        if not os.path.exists("__cache"):
            os.makedirs("__cache")
    def __setitem__(this, key, value):
        this.map[hash(key)] = {}
        this.map[hash(key)]["data"] = value
        this.map[hash(key)]["access"] = time.time()
        return this.map[hash(key)]["data"]
    def __getitem__(this, key):
        if os.path.isfile("__cache/{}".format(hash(key))) and hash(key) not in this.map:
            with open("__cache/{}".format(hash(key)), "r") as file:
                read = json.load(file)
                read["access"] = time.time()
                this[hash(key)] = read
        try:
            this.map[hash(key)]["access"] = time.time()
            return this.map[hash(key)]["data"]
        except:
            return None
    def __delitem__(this, key):
        if hash(key) in this.map:
            del this.map[hash(key)]
        return None
    def store(this):
        if not os.path.exists("__cache"):
            os.makedirs("__cache")
        for index in this.map:
            with open("__cache/{}".format(str(index)), "w") as file:
                json.dump(this.map[index], file)
    def clear(this):
        this.map = {}
    def trim(this):
        list = sorted([x for x in this.map], key = lambda x : this.map[x]["access"])
        for i in range(len(this.map), this.upperLimit, -1):
            del this[list[i-1]]
    def monitor(this):
        while True:
            if this.monitorFlag:
                break
            if len(this.map) > this.upperLimit:
                this.trim()
            time.sleep(this.delay)
